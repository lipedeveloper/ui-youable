import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {

  senha: string = localStorage.getItem('senha');
  porta: string = localStorage.getItem('porta');

  constructor() { }

  ngOnInit(): void {
  }


  Save() {
    localStorage.setItem('senha', this.senha);
    localStorage.setItem('porta', this.porta);
  }

}
