import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalCreateComponent } from '../_modal/youtube/modal-create/modal-create.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { YoutubeClasses } from '../_model/YoutubeClasses';
import { YoutubeService } from '../_service/youtube.service';
import { ModalDeleteComponent } from '../_modal/youtube/modal-delete/modal-delete.component';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {
  dataSource: MatTableDataSource<YoutubeClasses>;
  showTable: boolean;
  displayedColumns: string[] = ['thumbnail', 'name', 'detail', 'class_date', 'work_load', 'id'];
  blocked: boolean = false;

  
  constructor(private youtubeService: YoutubeService, public dialog: MatDialog, private _snackBar: MatSnackBar) {
  }
  

  ngOnInit(): void {
  }

  openModalDelete(id): void {
    const dialogRefDel = this.dialog.open(ModalDeleteComponent, {
      width: '25%',
      data: id
    });
    dialogRefDel.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.youtubeService.get().subscribe(i => {
          this.dataSource = new MatTableDataSource(i.data);
          this.showTable = true;
        });
      }, 1000 / 60);
    });
  }

  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {
    this.youtubeService.get().subscribe(i => {
      this.dataSource = new MatTableDataSource(i.data);
      this.showTable = true;
    });
  }

  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });
  }

  openModal(): void {
    const dialogRef = this.dialog.open(ModalCreateComponent, {
      width: '50%',
      disableClose: true 
    });

    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.youtubeService.get().subscribe(i => {
          this.dataSource = new MatTableDataSource(i.data);
          this.showTable = true;
        });
      }, 1000 / 60);
    });

  }


}
