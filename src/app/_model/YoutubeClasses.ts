import { YoutubeCategory } from './YoutubeCategory';
  
export class YoutubeClasses {
    Name: string;
    Detail: string;
    ClassDate: string;
    Thumbnail: string;
    WorkLoad: string;
    YoutubeVideosCategory: YoutubeCategory[];

    
    public constructor(name: string, detail: string, thumbnail: string, workLoad: string, youtubeVideosCategory: YoutubeCategory[]){
        this.Name = name;
        this.Detail = detail;
        this.Thumbnail = thumbnail;
        this.WorkLoad = workLoad;
        this.YoutubeVideosCategory = youtubeVideosCategory;
    }

 
    

}


