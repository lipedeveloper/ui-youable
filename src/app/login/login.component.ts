import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LoginService } from '../_service/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  profileForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });
  constructor(private loginService: LoginService, private router: Router, private _snackBar: MatSnackBar) { }

  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loginService.singIn(this.profileForm.value.email, this.profileForm.value.password)
      .subscribe(
        (response) => {
          if (response.success)
            this.router.navigate(['pages']);
          else
            this.openSnackBar(response.message);

        },
        error => (err => { console.log(err) })
      );
  }

}
