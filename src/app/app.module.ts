import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { PagesComponent } from './pages/pages.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ConfigurationComponent } from './configuration/configuration.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from './_service/login.service';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './_service/users.service';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ModalCreateComponent as UserModalComponentCreate } from './_modal/users/modal-create/modal-create.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ModalDeleteComponent as UserModalComponentDelete } from './_modal/users/modal-delete/modal-delete.component';
import { ModalDeleteComponent as YoutubeModalComponentDelete } from './_modal/users/modal-delete/modal-delete.component';
import { NgxMaskModule } from 'ngx-mask'
import { MatTabsModule } from '@angular/material/tabs';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ModalUpdateComponent } from './_modal/users/modal-update/modal-update.component';
import { MatSelectModule } from '@angular/material/select';
import { VideosComponent } from './videos/videos.component';
import { YoutubeService } from './_service/youtube.service';
import { ModalCreateComponent as YoutubeModalComponentCreate } from './_modal/youtube/modal-create/modal-create.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashBoardComponent,
    PagesComponent,
    ConfigurationComponent,
    SnackBarComponent,
    UsersComponent,
    UserModalComponentCreate,
    UserModalComponentDelete,
    ModalUpdateComponent,
    VideosComponent,
    YoutubeModalComponentCreate,
    YoutubeModalComponentDelete    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    MatCardModule,
    HttpClientModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    NgxMaskModule.forRoot(),
    MatSlideToggleModule,
    MatSelectModule,
    MatStepperModule,
    MatDividerModule
  ],
  providers: [LoginService, UsersService, YoutubeService, MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
