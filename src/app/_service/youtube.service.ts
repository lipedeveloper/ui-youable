import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResponseBase } from '../_model/responseBase';
import { YoutubeClasses } from '../_model/YoutubeClasses';

@Injectable()
export class YoutubeService {
  private readonly ApiURL = `${environment.apiURL}/Youtube`;

  constructor(private http: HttpClient) { }

  get() {
    return this.http.get<any>(this.ApiURL + '/show') as Observable<ResponseBase<YoutubeClasses[]>>;
  }

  create(youtube: YoutubeClasses[]): Observable<ResponseBase<boolean>> {
    return this.http.post<any>(this.ApiURL, youtube, { "headers": this.getHeader() }) as Observable<ResponseBase<boolean>>;
  }

  delete(id: number) {
    return this.http.request('delete', this.ApiURL + '/' + id, { body: id, headers: this.getHeader() });
  }

  getHeader(contentType = 'application/json'): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', contentType);
    headers = headers.set('Accept', contentType);
    headers = headers.set('Access-Control-Allow-Origin', '*');
    headers = headers.set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    headers = headers.set('X-Requested-With', 'XMLHttpRequest');
    return headers;
  }

}


