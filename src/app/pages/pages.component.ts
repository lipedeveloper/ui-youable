import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../_service/login.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {
  public username: string;

  constructor(private router: Router, private loginService: LoginService) {
    this.username = this.loginService.getLocalStorage("name");
  }

  ngOnInit(): void {
  }


  Logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }

}
