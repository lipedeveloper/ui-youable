import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResponseBase } from '../_model/responseBase';
import { User } from '../_model/User';

@Injectable()
export class UsersService {
  private readonly ApiURL = `${environment.apiURL}/User`;

  constructor(private http: HttpClient) { }

  get($deleted = 0) {
    return this.http.get<any>(this.ApiURL + '/' + $deleted) as Observable<ResponseBase<User[]>>;
  }

  create(user: User): Observable<ResponseBase<User>> {
    return this.http.post<any>(this.ApiURL, user, { "headers": this.getHeader() }) as Observable<ResponseBase<User>>;
  }

  delete(id: number) {
    return this.http.request('delete', this.ApiURL + '/' + id, { body: id, headers: this.getHeader() });
  }

  blockedUser(id: any, state: boolean) {
    return this.http.request('put', this.ApiURL + '/' + id + '/' + state, { body: id, headers: this.getHeader() });
  }

  getHeader(contentType = 'application/json'): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', contentType);
    headers = headers.set('Accept', contentType);
    headers = headers.set('Access-Control-Allow-Origin', '*');
    headers = headers.set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    headers = headers.set('X-Requested-With', 'XMLHttpRequest');
    return headers;
  }

}


