import { YoutubeVideos } from './YoutubeVideos';
 
export class YoutubeCategory {
    Name: string;
    Detail: string;
    Thumbnail: string;
    YoutubeVideos: YoutubeVideos[];

       
    public constructor(name: string, detail: string, thumbnail: string, youtubeVideos: YoutubeVideos[]){
        this.Name = name;
        this.Detail = detail;
        this.Thumbnail = thumbnail;
        this.YoutubeVideos = youtubeVideos;
    }
 
}
