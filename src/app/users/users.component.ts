import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../_service/users.service';
import { User } from '../_model/User';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalCreateComponent } from '../_modal/users/modal-create/modal-create.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { ModalDeleteComponent } from '../_modal/users/modal-delete/modal-delete.component';
import { ModalUpdateComponent } from '../_modal/users/modal-update/modal-update.component';
import { RequesBlocked } from '../_modal/users/modal-update/requesBlocked';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  dataSource: MatTableDataSource<User>;
  showTable: boolean;
  displayedColumns: string[] = ['picture', 'name', 'email', 'birthDay', 'gender', 'id'];
  blocked: boolean = false;



  constructor(private userService: UsersService, public dialog: MatDialog, private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  openModalUpdateBlocked(id: any, state: any) {
    const requestBlocked = new RequesBlocked(id, state);
    const dialogRefUpBlocked = this.dialog.open(ModalUpdateComponent, {
      width: '25%',
      data: requestBlocked
    });
    dialogRefUpBlocked.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.userService.get(this.blocked ? 0 : 1).subscribe(i => {
          this.dataSource = new MatTableDataSource(i.data);
          this.showTable = true;
        });
      }, 1000 / 60);
    });
  }

  openModal(): void {
    const dialogRef = this.dialog.open(ModalCreateComponent, {
      width: '33%',
    });

    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.userService.get(this.blocked ? 0 : 1).subscribe(i => {
          this.dataSource = new MatTableDataSource(i.data);
          this.showTable = true;
        });
      }, 1000 / 60);
    });

  }

  openModalDelete(id): void {
    const dialogRefDel = this.dialog.open(ModalDeleteComponent, {
      width: '25%',
      data: id
    });
    dialogRefDel.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.userService.get(this.blocked ? 0 : 1).subscribe(i => {
          this.dataSource = new MatTableDataSource(i.data);
          this.showTable = true;
        });
      }, 1000 / 60);
    });
  }


  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });
  }

  usersBlocked() {
    this.userService.get(this.blocked ? 0 : 1).subscribe(i => {
      this.dataSource = new MatTableDataSource(i.data);
      this.showTable = true;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {
    this.userService.get(this.blocked ? 0 : 1).subscribe(i => {
      this.dataSource = new MatTableDataSource(i.data);
      this.showTable = true;
    });
  }


}




