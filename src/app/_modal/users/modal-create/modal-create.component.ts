import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsersService } from 'src/app/_service/users.service';
import { User } from 'src/app/_model/User';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from 'src/app/snack-bar/snack-bar.component';

@Component({
  selector: 'app-modal-create',
  templateUrl: './modal-create.component.html',
  styleUrls: ['./modal-create.component.css']
})
export class ModalCreateComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private userService: UsersService, private _snackBar: MatSnackBar) {
  }
  name: string;
  email: string;
  password: string;
  picture: string = "https://mir-s3-cdn-cf.behance.net/project_modules/disp/ce54bf11889067.562541ef7cde4.png";
  birthDay: string;
  gender: string;
  genders: string[] = ['Male', 'Female'];

  success: boolean;

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCreateClick() {
    var user = new User(this.name, this.email, this.password, this.picture, this.transFormDate(this.birthDay), this.gender);
    if (!user.validate()) {
      this.openSnackBar("Preencha todos os dados!");
    }else{

        this.userService.create(user).subscribe(i => {
          if (i.success) {
            this.openSnackBar(i.message);
            this.dialogRef.close();
          } else {
            this.openSnackBar(i.message);
          }

        });

     }
  }


  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });
  }

  // TODO - Refazer
  transFormDate(date: string) {

    if (date == null || date == undefined || date == "") {
      return "0000-00-00";
    }

    var d = date.substr(0, 2);
    var m = date.substr(2, 2);
    var y = date.substr(4, 4);


    return y + "/" + m + "/" + d;
  }

  ngOnInit(): void {
  }

}
