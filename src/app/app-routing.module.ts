import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { PagesComponent } from './pages/pages.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { UsersComponent } from './users/users.component';
import { VideosComponent } from './videos/videos.component';


const routes: Routes = [
  {
    path: 'pages', component: PagesComponent, children: [
      { path: 'pages/', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashBoardComponent },
      { path: 'configuration', component: ConfigurationComponent },
      { path: 'users', component: UsersComponent },
      { path: 'videos', component: VideosComponent },
    ]
  },
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
