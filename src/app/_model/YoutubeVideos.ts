export class YoutubeVideos {
    Name: string;
    Source: string;
    Type: number;

    constructor(name: string, source:string, type: number){
        this.Name = name;
        this.Source = source;
        this.Type = type;
    }
}
