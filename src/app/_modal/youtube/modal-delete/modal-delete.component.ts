import { Component, OnInit, Inject } from '@angular/core';
import { YoutubeService } from 'src/app/_service/youtube.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackBarComponent } from 'src/app/snack-bar/snack-bar.component';

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.css']
})
export class ModalDeleteComponent implements OnInit {

 
  constructor(
    public dialogRefDel: MatDialogRef<ModalDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private youtubeService: YoutubeService, private _snackBar: MatSnackBar) {
  }
  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRefDel.close();
  }

  delete() {
    this.youtubeService.delete(this.data).subscribe(i => {
      if (i) {
        this.openSnackBar("Aula deletada com sucesso!");
        this.dialogRefDel.close();
      }
    });
  }

  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });


  }
}
