import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsersService } from 'src/app/_service/users.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackBarComponent } from 'src/app/snack-bar/snack-bar.component';
import { RequesBlocked } from './requesBlocked';


@Component({
  selector: 'app-modal-update',
  templateUrl: './modal-update.component.html',
  styleUrls: ['./modal-update.component.css']
})
export class ModalUpdateComponent implements OnInit {

  constructor(
    public dialogRefUpBlocked: MatDialogRef<ModalUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RequesBlocked, private userService: UsersService, private _snackBar: MatSnackBar
  ) { }
  state: any;
  ngOnInit(): void {
    this.state = this.data.state ? 'desbloqueado' : 'bloqueado';
  }

  onNoClick(): void {
    this.dialogRefUpBlocked.close();
  }


  updateBlocked() {
    this.userService.blockedUser(this.data.id, this.data.state).subscribe(i => {
      if (i) {

        this.openSnackBar("Usuário " + this.state + " com sucesso!");
        this.dialogRefUpBlocked.close();
      }
    });
  }

  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });

  }

}
