export class User {
    name: string;
    email: string;
    password: string;
    picture: string;
    birthDay: string;
    gender: string;

    constructor(name: string, email: string, password: string, picture: string, birthDay: string, gender: string) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.birthDay = birthDay;
        this.gender = gender;
    }

    validate(): boolean {
        return (this.name != null && this.email != null && this.password != null && this.picture != null && this.birthDay != null && this.gender != null);
    }
}
