import { Component, OnInit, Inject } from '@angular/core';
import { YoutubeService } from 'src/app/_service/youtube.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { YoutubeClasses } from 'src/app/_model/YoutubeClasses';
import { YoutubeCategory } from 'src/app/_model/YoutubeCategory';
import { YoutubeVideos } from 'src/app/_model/YoutubeVideos';
import { SnackBarComponent } from 'src/app/snack-bar/snack-bar.component';

@Component({
  selector: 'app-modal-create',
  templateUrl: './modal-create.component.html',
  styleUrls: ['./modal-create.component.css']
})
export class ModalCreateComponent implements OnInit {
  isLinear = false;
  youtubeClasses: FormGroup;
  youtubeCategory: FormGroup;
  youtubeVideos: FormGroup;
  
  arrayItems: {
    id: number;
    title: string;
  }[];
  
  constructor(
    public dialogRef: MatDialogRef<ModalCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private youtubeService: YoutubeService, private _snackBar: MatSnackBar, private _formBuilder: FormBuilder) {
  }
 

  ngOnInit(): void {
    this.youtubeClasses = this._formBuilder.group({
      name: ['', Validators.required],
      detail: ['', Validators.required],
      thumbnail: ['', Validators.required],
      work_load: ['', Validators.required]
    });


    this.youtubeCategory = this._formBuilder.group({
      items: this._formBuilder.array([this.createCategoryItem()])
    });

    this.youtubeVideos = this._formBuilder.group({
      name: ['', Validators.required],
      source: ['', Validators.required],
      type: ['', Validators.required],
    });
 
  }
 

  createCategoryItem() {
    return this._formBuilder.group({
      name: ['', Validators.required],
      detail: ['', Validators.required],
      thumbnail: ['', Validators.required],
      youtubeVideos: this._formBuilder.array([this.createVideoItem()])
    })
  }

  createVideoItem(){
    return this._formBuilder.group({
      name: ['', Validators.required],
      source: ['', Validators.required],
      type: ['', Validators.required],
    })
  }
 

   removeCategory(i: number) {
      const control = <FormArray>this.youtubeCategory.get('items');
      control.removeAt(i);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  removeVideo(itemIndex,videoIndex){
    const control = ((this.youtubeCategory.get('items') as FormArray).controls[itemIndex].get('youtubeVideos') as FormArray);
    control.removeAt(videoIndex);
  }
 
  addCategory() {
    (this.youtubeCategory.controls['items'] as FormArray).push(this.createCategoryItem())
  }

  addVideo(form){
    var control = form.controls.youtubeVideos.controls;
    control.push(this.createVideoItem());
  }

  getCategoryControls() {
    return (this.youtubeCategory.get('items') as FormArray).controls;
  }

  getVideosControl(form){   
    return form.controls.youtubeVideos.controls;
  }

  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });
  }

  sendClass(){  
    
    var classes = this.youtubeClasses.value;  
    var category = this.youtubeCategory.value;

    var youtubeCategoryArr = new Array<YoutubeCategory>();

    category.items.forEach(category => {     

      var youtubeVideosArr = new Array<YoutubeVideos>();
      category.youtubeVideos.forEach(videos => {
        var youtubeVideos = new YoutubeVideos(videos.name, videos.source, videos.type);        
        youtubeVideosArr.push(youtubeVideos);
      });      
      var youtubeCategory = new YoutubeCategory(category.name,category.detail, category.thumbnail, youtubeVideosArr);
      youtubeCategoryArr.push(youtubeCategory);
    });
    
    var youtubeClasses = new YoutubeClasses(classes.name, classes.detail, classes.thumbnail, classes.work_load, youtubeCategoryArr);
 
    this.youtubeService.create([youtubeClasses]).subscribe(i => {
      if (i.success) {
        this.openSnackBar(i.message);
        this.dialogRef.close();
      } else {
        this.openSnackBar(i.message);
      }

    });
  }

}
